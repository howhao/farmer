<?php
require '../__connect_db.php';
if (!isset($_POST['name'])) {
    exit;
}
$result = [
    'success' => false,
    'code' => 400,
    'info' => 'No Insert',
    'post' => $_POST
];



$sql = "INSERT INTO `farmers`( `company`,
 `name`, `email`, `password`, `mobile`, `created_at`) VALUES (?,?,?,?,?,NOW())";

$stmt = $pdo->prepare($sql);
$stmt->execute([
    $_POST['company'],
    $_POST['name'],
    $_POST['email'],
    $_POST['password'],
    $_POST['mobile']
    ]);

if($stmt->rowCount()==1){
    $result['success'] = true;
    $result['code'] = 200;
    $result['info'] = "Success";
    }else{
        $result['code'] = 420;
        $result['info'] = "Fail";
    };


echo json_encode($result, JSON_UNESCAPED_UNICODE);

?>