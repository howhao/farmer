<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="../assets/img/webicon.png">
    <!--掛-->
    <link type=text/css href="https://fonts.googleapis.com/css?family=Muli:400,300" rel=stylesheet>
    <link type=text/css href="https://fonts.googleapis.com/css?family=Montserrat" rel=stylesheet>
    <link rel="stylesheet" href="../assets/css/bootstrap.css">
<!--    <link rel="stylesheet" href="../assets/css/argon-dashboard.css">-->
    <link rel="stylesheet" href="../assets/fontawesome/css/all.css">
    <link href=../assets/static/css/themify-icons.css rel=stylesheet>
    <link href=../assets/static/css/app.af03ab8b65bad0382054e44d0c417e73.css rel=stylesheet>
    <script src="../assets/js/jquery-3.4.1.js"></script>
    <title>SignIn</title>
</head>
<style>
    body{
        background: #f5f5f5;
    }
    .form-control{
        background-color: #ffffff;
    }
    small.form-text {
        color: red;
        transition: 1s;
        height: 10px;

    }

    input {
        transition: 1s;

    }
    /*.bg-gradient-danger {*/
    /*    background: linear-gradient(87deg, #f5365c 0, #f56036 100%) !important;*/
    /*}*/
    .bg-gradient-danger {
        background: linear-gradient(87deg, #007e71 0, #2BBBAD 100%) !important;
    }

    .modal-danger .modal-content {
         background-color: #f5365c;
         color: #fff;
        border: none;
     }
    .modal-header{
        border-bottom:none;
    }
    .modal-footer{
        border-top:none;
    }
    .close{
        text-shadow: none;
    }
</style>
<body >

<div class="" >

    <!-- <div class="notifications vue-notifyjs"><span mode="out-in"></span></div> -->
    <div>
        <nav class="navbar navbar-absolute">
            <div class=" navbar-collapse">
                <div class="navbar-header logo">
                    <a href="#" style="color: #252422;" class="navbar-brand">OrganicFoo</a>
                    <a href="login.php" class="btn m-4 float-right" >
                        Looking to login?
                    </a>
                </div>
            </div>
        </nav>
        <div class="wrapper wrapper-full-page">
            <div class="register-page">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="header-text">
                                    <h2>Create Free Account</h2>
                                    <h4>Register for free and experience today.</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="col-md-12 d-flex justify-content-center">
                            <div class="col-md-4 ">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon icon-danger"><i class="ti ti-user"></i></div>
                                    </div>
                                    <div class="media-body">
                                        <h5>Free Account</h5>
                                        Here you can write a feature description for your dashboard, let the users know
                                        what is the value that you give them.
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon icon-warning"><i class="ti-bar-chart-alt"></i></div>
                                    </div>
                                    <div class="media-body">
                                        <h5>Awesome Performances</h5>
                                        Here you can write a feature description for your dashboard, let the users know
                                        what is the value that you give them.
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <div class="icon icon-info"><i class="ti-headphone"></i></div>
                                    </div>
                                    <div class="media-body">
                                        <h5>Global Support</h5>
                                        Here you can write a feature description for your dashboard, let the users know
                                        what is the value that you give them.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <form name="form1" onsubmit="return checkForm()">
                                    <div class="card card-plain" style="border: none">
                                        <div class="content">
                                            <div class="form-group position-relative">
                                                <input type="text" id="name" placeholder="Your Name" class="form-control"
                                                       name="name">
                                                <small id="nameHelp" class="form-text float-left"></small>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="company" placeholder="Company" class="form-control"
                                                       name="company">
                                                <small id="companyHelp" class="form-text float-left "></small>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="mobile" placeholder="Mobile" class="form-control"
                                                       name="mobile">
                                                <small id="mobileHelp" class="form-text float-left "></small>
                                            </div>
                                            <div class="form-group">
                                                <input type="email" id="email" placeholder="Enter email" class="form-control"
                                                       name="email">
                                                <small id="emailHelp" class="form-text float-left "></small>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" id="password" placeholder="Password" class="form-control"
                                                       name="password">
                                                <small id="passwordHelp" class="form-text float-left "></small>
                                            </div>
                                            <div class="form-group">
                                                <input type="password" id="confirm" placeholder="Password Confirmation"
                                                       class="form-control">
                                                <small id="confirmHelp" class="form-text float-left "></small>
                                            </div>
                                        </div>
                                        <div class="footer text-center">
                                            <button type="submit" class="btn btn-fill btn-danger btn-wd" id="submit_btn">Create Free
                                                Account
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div></div>
                    </div>
                </div>
                <footer class="footer footer-transparent fixed-bottom">
        <div class="container">
            <div class="text-center">
                © Coded with
                <i class="fas fa-carrot"></i> by
                <a href="#" target="_blank">Sunny</a>. Designed by <a href="#" target="_blank">SSS</a>.
            </div>
        </div>
    </footer>
            </div>
        </div>
        <div class="collapse navbar-collapse off-canvas-sidebar">
            <ul class="nav nav-mobile-menu">
                <li><a href="#/login" class="">
                        Looking to login?
                    </a></li>
                <li><a href="#/admin" class="">
                        Dashboard
                    </a></li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-4">
    <button type="button" hidden id="notify_btn" class="btn btn-block btn-warning mb-3" data-toggle="modal" data-target="#modal-notification">Notification</button>
    <div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
        <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
            <div class="modal-content bg-gradient-danger">

                <div class="modal-header">
                    <h6 class="modal-title" id="modal-title-notification">Hello&nbsp&nbsp</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="py-3 text-center">
                        <i class="ni ni-bell-55 ni-3x"></i>
                        <h4 class="heading mt-4">Thank you for Register!</h4>
                        <p>We will notice you when the account is passed</p>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn text-white" >Contact Us</button>
                    <button type="button" class="btn text-white ml-auto" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    let i, s, item;
    const required_fields = [
        {
            id: 'name',
            pattern: /^\S{2,}/,
            info: 'What Your Name Again?'
        },
        {
            id: 'company',
            pattern: /^\S{2,}/,
            info: 'Type The Correct Company '
        },
        {
            id: 'email',
            pattern: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
            info: 'Type The Correct Email'
        },
        {
            id: 'mobile',
            pattern: /^09\d{2}\-?\d{3}\-?\d{3}$/,
            info: 'I Will Call You'
        },
        {
            id: 'password',
            pattern: /^\d{4,8}$/,
            info: 'Type The Correct password'
        },
        {
            id: 'confirm',
            pattern: /^\d{4,8}$/,
            info: 'Type The Correct password'
        },


    ];
    // 拿到對應的 input element (el), 顯示訊息的 small element (infoEl)
    for (s in required_fields) {
        item = required_fields[s];
        item.el = document.querySelector('#' + item.id);
        item.infoEl = document.querySelector('#' + item.id + 'Help');

    };


    function checkForm() {
        let account_name =  $('#name').val();

        // 先讓所有欄位外觀回復到原本的狀態
        for (s in required_fields) {
            item = required_fields[s];
            item.el.style.border = '1px solid #dddddd';
            item.infoEl.innerHTML = '';
        }
        submit_btn.style.display = 'none';
        // 檢查必填欄位, 欄位值的格式

        let isPass = true;
        for (s in required_fields) {

            var password;
            item = required_fields[s];
            if (item.id == 'password') {
                password = item.el.value;

            }

            if (!item.pattern.test(item.el.value)) {
                item.el.style.border = '1px solid red';
                item.infoEl.innerHTML = item.info;

                isPass = false;
                setTimeout(() => {
                    submit_btn.style.display = 'block';
                }, 1500)
            }
            if (item.id == 'confirm') {
                if (item.el.value !== password) {
                    console.log(item.el.value);
                    item.el.style.border = '1px solid red';
                    item.infoEl.innerHTML = 'Does not match';
                    isPass = false;
                    setTimeout(() => {
                        submit_btn.style.display = 'block';
                    }, 1500)
                }

            }
        }



        let fd = new FormData(document.form1);
        if (isPass) {
            fetch('Signin_api.php', {
                method: 'POST',
                body: fd
            })
                .then(response => {
                    if(response.status==500){
                        console.log(response.status)
                        console.log('fail')
                    let emailHelp = document.querySelector('#emailHelp');
                    let email = document.querySelector('#email');
                    email.style.border = '1px solid red';
                    emailHelp.innerHTML = 'Email Already Exist';
                    submit_btn.style.display = 'block';
                    }else{
                        return response.json()
                    console.log(response)
                    console.log('success')
                    }

                })
                .then(json=>{
                    if(json.success){
                        console.log(json)
                        submit_btn.style.display = 'block';
                        $('#modal-title-notification').append(account_name);
                        $('#notify_btn').click();

                    }else{
                        console.log(json)
                        let emailHelp = document.querySelector('#emailHelp');
                        let email = document.querySelector('#email');
                        email.style.border = '1px solid red';
                        emailHelp.innerHTML = 'Email Already Exist';
                        submit_btn.style.display = 'block';
                    }

                })

        }

        return false; // 表單不出用傳統的 post 方式送出
    }

</script>
<script src="../assets/js/popper.min.js"></script>
<!--<script src="../assets/js/argon-dashboard.js"></script>-->
<script src="../assets/js/bootstrap.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="../assets/js/dropzone.js" ></script>
<script src="../assets/js/paper-dashboard.js?v=2.0.0" type="text/javascript"></script>
</body>
