<?php
if(! isset($_SESSION)){
    session_start();
}
if(! isset($_SESSION['loginFarmer'])){
    header('Location: ../Admin/login.php');
    exit;
}