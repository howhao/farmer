-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- 主機： localhost:8889
-- 產生時間： 2019 年 09 月 08 日 03:32
-- 伺服器版本： 5.7.26
-- PHP 版本： 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- 資料庫： `organicfood`
--

-- --------------------------------------------------------

--
-- 資料表結構 `farmers`
--

CREATE TABLE `farmers` (
  `farmer_id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `storename` varchar(255) DEFAULT NULL,
  `taxid` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `aboutme` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `farmers`
--

INSERT INTO `farmers` (`farmer_id`, `company`, `storename`, `taxid`, `name`, `email`, `password`, `telephone`, `mobile`, `address`, `nickname`, `aboutme`, `img`, `color`, `status`, `created_at`) VALUES
(5, 'farm25', 'OrganFood25', '26109455', '王力宏5', '1234@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', '[\"2d3347a52d3deec4c7d7d503abecd3c6.jpg\",\"59338d732af450aa5542b2883b5936f8.jpg\"]', NULL, 1, '2019-09-07 21:07:24'),
(7, 'farm27', 'OrganFood27', '26109457', '王力宏7', 'wang7@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:43:16'),
(9, 'farm29', 'OrganFood29', '26109459', '王力宏9', 'wang9@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 17:45:04'),
(11, 'farm211', 'OrganFood211', '261094511', '王力宏11', 'wang11@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:43:17'),
(12, 'farm212', 'OrganFood212', '261094512', '王力宏12', 'wang12@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:43:18'),
(13, 'farm213', 'OrganFood213', '261094513', '王力宏13', 'wang13@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:43:19'),
(14, 'farm214', 'OrganFood214', '261094514', '王力宏14', 'wang14@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:43:20'),
(15, 'farm215', 'OrganFood215', '261094515', '王力宏15', 'wang15@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:43:21'),
(16, 'farm216', 'OrganFood216', '261094516', '王力宏16', 'wang16@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:43:27'),
(17, 'farm217', 'OrganFood217', '261094517', '王力宏17', 'wang17@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:43:27'),
(18, 'farm218', 'OrganFood218', '261094518', '王力宏18', 'wang18@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:54:20'),
(19, 'farm219', 'OrganFood219', '261094519', '王力宏19', 'wang19@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:54:21'),
(20, 'farm220', 'OrganFood220', '261094520', '王力宏20', 'wang20@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:54:04'),
(21, 'farm221', 'OrganFood221', '261094521', '王力宏21', 'wang21@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:54:05'),
(22, 'farm222', 'OrganFood222', '261094522', '王力宏22', 'wang22@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:54:22'),
(23, 'farm223', 'OrganFood223', '261094523', '王力宏23', 'wang23@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 17:44:30'),
(24, 'farm224', 'OrganFood224', '261094524', '王力宏24', 'wang24@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:54:07'),
(25, 'farm225', 'OrganFood225', '261094525', '王力宏25', 'wang25@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 00:54:25'),
(26, 'farm226', 'OrganFood226', '261094526', '王力宏26', 'wang26@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:23'),
(27, 'farm227', 'OrganFood227', '261094527', '王力宏27', 'wang27@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:23'),
(28, 'farm228', 'OrganFood228', '261094528', '王力宏28', 'wang28@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:24'),
(29, 'farm229', 'OrganFood229', '261094529', '王力宏29', 'wang29@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 10:26:44'),
(30, 'farm230', 'OrganFood230', '261094530', '王力宏30', 'wang30@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:24'),
(31, 'farm231', 'OrganFood231', '261094531', '王力宏31', 'wang31@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 10:26:42'),
(32, 'farm232', 'OrganFood232', '261094532', '王力宏32', 'wang32@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:17'),
(33, 'farm233', 'OrganFood233', '261094533', '王力宏33', 'wang33@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:17'),
(34, 'farm234', 'OrganFood234', '261094534', '王力宏34', 'wang34@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:17'),
(35, 'farm235', 'OrganFood235', '261094535', '王力宏35', 'wang35@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:17'),
(36, 'farm236', 'OrganFood236', '261094536', '王力宏36', 'wang36@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:17'),
(37, 'farm237', 'OrganFood237', '261094537', '王力宏37', 'wang37@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:17'),
(40, 'farm240', 'OrganFood240', '261094540', '王力宏40', 'wang40@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:18'),
(41, 'farm241', 'OrganFood241', '26109454', '王力宏41', 'wang41@gmail.com', '12345678', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', '[\"cfc4078f6e8cca8578598a17e135db21.jpg\",\"5bf74d29538d7d803f629abb1601d8d5.jpg\"]', NULL, 1, '2019-09-06 16:37:46'),
(42, 'farm242', 'OrganFood242', '261094542', '王力宏42', 'wang42@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 17:32:16'),
(43, 'farm243', 'OrganFood243', '261094543', '王力宏43', 'wang43@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 16:44:18'),
(44, 'farm244', 'OrganFood244', '261094544', '王力宏44', 'wang44@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 10:24:11'),
(45, 'farm245', 'OrganFood245', '261094545', '王力宏45', 'wang45@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-07 10:26:37'),
(46, 'farm246', 'OrganFood246', '261094546', '王力宏46', 'wang46@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 17:21:59'),
(48, 'farm248', 'OrganFood248', '261094548', '王力宏48', 'wang48@gmail.com', '1234', '02-2322-1234', '0933444555', 'Taipei', 'wang', 'Aboutme', NULL, NULL, 1, '2019-09-06 17:21:59'),
(50, 'Farm1', 'PPP', '23456545', 'Sunny', 'ssss44@gmail.com', '1928485', '02-2233-2111', '0912555777', 'Taipei', 'wangyo0101', 'Aboutme', '[\"b7954daffb2ad14a41f175acdd8a9a9a.jpg\",\"9499df116ee745b7a9690851626734c1.jpg\"]', NULL, 1, '2019-09-06 17:21:57'),
(51, 'farmyoyo', NULL, NULL, 'Sunny', 'ss17s44@gmail.com', '1234', NULL, '0912555777', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 17:21:58'),
(54, 'Beatles', NULL, NULL, '黃小明50', 'sunny@gmail.com', '1234', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:44:19'),
(57, 'Beatles', NULL, NULL, 'beatles', '12345@gmail.com', '12345', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:44:20'),
(58, 'Beatles', NULL, NULL, 'beatles', '1357@gmail.com', '1357', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-07 10:24:05'),
(59, 'Beatles', NULL, NULL, 'beatles', '1sunny1@gmail.com', '1234', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:44:14'),
(62, 'Beatles', NULL, NULL, '黃小明50', 'sunny125@gmail.com', '1234', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:42:31'),
(66, 'Beatles', NULL, NULL, '黃小明50', 's2unny1@gmail.com', '1234', NULL, '0954545433', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:44:14'),
(69, 'Beatles', NULL, NULL, 'beatles', 'su1nny1@gmail.com', '1234', NULL, '0954545422', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:44:13'),
(72, 'Beatles', NULL, NULL, '黃小明50', 'sunn1y2@gmail.com', '1234', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:44:13'),
(74, 'Beatles', 'MYFARM', '23456543', 'beatles', 's1u1nny1@gmail.com', '12345', '02-23435555', '0933222111', 'Taipei', 'beatless', 'What You Want', '[\"0dcd0d4d391a35cffe956a48c9a008ff.jpg\",\"904051e7182f3df0fb6c9eca0cfc127a.jpg\"]', '[\"black\",\"warning\"]', 1, '2019-09-06 16:44:24'),
(75, 'Beatles', NULL, NULL, 'beatles', 'yosyo1s@gmail.com', '1234', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:33:51'),
(76, 'BeatlesFarm', NULL, NULL, 'beatles123', 'olivia@gmail.com', '1234', NULL, '0933221898', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:33:59'),
(77, 'Beatles', NULL, NULL, 'beatles', '11sunny22@gmail.com', '1234', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:44:26'),
(78, 'Beatles', NULL, NULL, 'beatles', '11sunny23@gmail.com', '1234', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:44:26'),
(79, 'Beatles', NULL, NULL, 'beatles', 's1unn1y@gmail.com', '1234', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 17:32:26'),
(80, 'Beatles', NULL, NULL, '黃小明50', 'sunny126@gmail.com', '1234', NULL, '0933222111', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 16:33:48'),
(81, 'Beatles123', NULL, NULL, 'beatles123', 'beatles551@gmail.com', '1234', NULL, '0933222555', NULL, NULL, NULL, NULL, NULL, 1, '2019-09-06 17:32:03');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `farmers`
--
ALTER TABLE `farmers`
  ADD PRIMARY KEY (`farmer_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `farmers`
--
ALTER TABLE `farmers`
  MODIFY `farmer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
