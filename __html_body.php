<script>

$( document ).ready(function() {
    let welcome = $('#welcome');
    fetch('https://api.openweathermap.org/data/2.5/weather?q=Taipei&appid=500b5fc68b89851ff95541b5e46645a3')
    .then(response=>{
        console.log(response);
        return response.json();
    })
    .then(json=>{
        console.log(json.weather[0].main);
        welcome.append(`${json.name} &nbsp &nbsp${(json.main.temp-273.15).toFixed(1)}&deg;C &nbsp &nbsp${json.weather[0].description}`)
    })
});
let present = new Date();
let present_day = `${present.getDay()}`;
</script>
<?php
$sql_tt = "SELECT `color` FROM `admin` WHERE `email`=?";

$stmt_tt = $pdo->prepare($sql_tt);

$stmt_tt->execute([
    $_SESSION['loginFarmer']['email']
]);
$row_tt = $stmt_tt->fetch();
if(empty($row_tt)){
    $color = [];
}else{
    $color = json_decode($row_tt['color']);
}


?>

<body>
    <div class="wrapper ">
        <div class="sidebar" data-color="<?= empty($color[0])? "white" : $color[0] ?>"
            data-active-color="<?= empty($color[1])? "danger" : $color[1] ?>">
            <!--
Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
-->
            <div class="logo">
                <a href="#" class="simple-text logo-mini">
                    <div class="logo-image-small">
                        <img src="../assets/img/logo_ol.svg">
                    </div>
                </a>
                <a href="#" class="simple-text logo-normal">
                   FristFood
                    <!-- <div class="logo-image-big">
    <img src="../assets/img/logo-big.png">
  </div> -->
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="<?= $page_name == "adminhome" ? 'active' : ''; ?>">
                        <a href="../Admin/adminHome.php">
                            <i class="nc-icon nc-bank"></i>
                            <p>Home</p>
                        </a>
                    </li>

                    
                    <li data-toggle="collapse" data-target="#product"
                        class="collapsed <?= ( $page_name == "farmer_product_read" or $page_name == "farmer_product_edit" or $page_name == "farmer_product_insert"or $page_name == "farmer_product_readon"or $page_name == "farmer_product_readout") ? 'active' : ''; ?>">
                        <a href="#">
                            <i class="nc-icon nc-basket"></i>
                            <p>商品</p><span class="arrow"></span>
                        </a>
                        </a>
                    </li>

                    <ul class=" sub-menu <?= ($page_name == "farmer_product_read" or $page_name == "farmer_product_edit" or $page_name == "farmer_product_insert"or $page_name == "farmer_product_readon"or $page_name == "farmer_product_readout") ? '' : 'collapse'; ?>"
                        id="product">

                        <li><a href="../Farmer_product/farmer_product_read.php"
                                <?= ( $page_name == "farmer_product_read" or $page_name == "farmer_product_edit") ? 'style="color: orange"' : ''; ?>>
                                <p>商品列表</p>
                            </a></li>
                        <li><a href="../Farmer_product/farmer_product_readon.php"
                                <?= ( $page_name == "farmer_product_readon") ? 'style="color: orange"' : ''; ?>>
                                <p>上架</p>
                            </a></li>
                        <li><a href="../Farmer_product/farmer_product_readout.php"
                                <?= ( $page_name == "farmer_product_readout") ? 'style="color: orange"' : ''; ?>>
                                <p>下架</p>
                            </a></li>


                    </ul>
                    <li data-toggle="collapse" data-target="#farmer"
                        class="collapsed <?= ($page_name == "farmer" or $page_name == "farmer_edit" or $page_name == "farmer_create" ) ? 'active' : ''; ?>">
                        <a href="#">
                            <i class="nc-icon nc-basket"></i>
                            <p>小農資料</p><span class="arrow"></span>
                        </a>
                        </a>
                    </li>

                    <ul class=" sub-menu <?= ($page_name == "farmer" or $page_name == "farmer_edit" or $page_name == "farmer_create" ) ? '' : 'collapse'; ?>"
                        id="farmer">
                        <li><a href="../Farmers/farmer_edit.php"
                                <?= ($page_name == "farmer" or $page_name == "farmer_edit" or $page_name == "farmer_create") ? 'style="color: orange"' : '' ?>>
                                <p>資料設定</p>
                            </a></li>
                    </ul>
                    <li data-toggle="collapse" data-target="#setting"
                        class="collapsed <?= ($page_name == "Account"or $page_name == "adminlayout") ? 'active' : ''; ?>">
                        <a href="#">
                            <i class="nc-icon nc-settings-gear-65"></i>
                            <p>Setting</p><span class="arrow"></span>
                        </a>
                        </a>
                    </li>

                    <ul class=" sub-menu <?= ($page_name == "Account" or $page_name == "adminlayout") ? '' : 'collapse'; ?>"
                        id="setting">
                        <li><a href="../Admin/admin_layout.php"
                                <?= $page_name == "adminlayout" ? 'style="color: orange"' : ''; ?>>
                                <p>Layout</p>
                            </a></li>
                    </ul>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <?php if($page_name=='adminhome'): ?>
                        <a id="welcome" style="letter-spacing:1px;transition:1s">
                        <!-- Hey , <p style="font-size:20px">
                        <?= $_SESSION['loginFarmer']['nickname']?> </p>
                        Today is <p style="font-size:20px"><?= date("l")?></p> </a> -->
                                <?php endif ?>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                        aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <!-- <form>
                        <div class="input-group no-border ">
                            <input type="text" value="" class="form-control" placeholder="Search...">
                            <div class="input-group-append">
                                <div class="input-group-text">

                                </div>
                            </div>
                        </div>
                    </form> -->
                        <ul class="navbar-nav">
                            <li class="nav-item btn-rotate dropdown">
                                <a class="nav-link" href="" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">

                                    <i class="nc-icon nc-bell-55 position-relative "
                                        style="filter: drop-shadow(5px 5px 20px 20px black)">
                                        <div class="position-absolute bell_notice" id="bell_notice" style=""></div>
                                    </i>
                                    <!-- <i class="far fa-bell fa-lg"></i> -->
                                    <!-- <i class="fas fa-bell fa-lg"></i> -->
                                    <p>
                                        <span class="d-lg-none d-md-block">Notice</span>
                                    </p>
                                </a>
                                <div id="notice-dropdown" class="dropdown-menu dropdown-menu-right"
                                    aria-labelledby="navbarDropdownMenuLink">
                                    <!-- <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a> -->
                                </div>
                            </li>
                            <li class="nav-item btn-rotate dropdown">
                                <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span><?= $_SESSION['loginFarmer']['nickname'] ?></span>
                                    <p>
                                        <span class="d-lg-none d-md-block"></span>
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="../Admin/admin_edit.php">Account Setting</a>
                                    <a class="dropdown-item" href="../Admin/logout.php">Log Out</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn-rotate" href="../Admin/admin_layout.php">
                                    <i class="nc-icon nc-settings-gear-65"></i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Setting</span>
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <script>
            
            </script>
            <?php include '../__html_breadCrumb.php' ?>